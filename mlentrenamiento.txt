cd Documents\Code\MLAgents\mla-1.0
mlagents-learn -h
mlagents-learn ./trainer_config.yaml --run-id hb_01


conda env list
conda activate ml-agents-1.0
cd Documents\Code\MLAgents\mla-1.0
tensorboard --logdir summaries
localhost:6006
