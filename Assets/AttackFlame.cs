using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackFlame : MonoBehaviour
{
    public GameObject Flame;
    float nextAttackTime = 0f;
    public float attackRatte = 2f;
    public PersonajeMovimiento Player;
    void Start()
    {
        //Player = FindObjectOfType<PersonajeMovimiento>();
    }
    void Update()
    {
        if (Time.time>=nextAttackTime)
        {
            if (Input.GetKeyDown(KeyCode.G))
            {
                //AttackF();
                nextAttackTime = Time.time + 1f / attackRatte;
            }
        }
    }
    public void AttackFlamebtn()
    {
        if (Time.time >= nextAttackTime)
        {
            Player.AttackFlame();
            nextAttackTime = Time.time + 1f / attackRatte;
        }
    }
    public void AttackMelebtn()
    {
        if (Time.time >= nextAttackTime)
        {
            Player.Attack();
            Player.AttackBoss();
            nextAttackTime = Time.time + 1f / attackRatte;
        }
    }
    public void jumpbtn()
    {
        if (Player.Ground)
        {
            Player.jump();
        }
    }
    public void MovDbtn()
    {
        Player.MovD();
    }
    public void MovAbtn()
    {
        Player.MovA();
    }
}
