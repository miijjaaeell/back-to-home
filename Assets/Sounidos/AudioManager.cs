using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public AudioMixer music, effets;
    public AudioSource Salto, caida,  vida, ATalEnemigo, DanoPlyer, ATaire,BackGround,MusicBoss,MurioPlayer;
    // Start is called before the first frame update
    public static AudioManager instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    void Start()
    {
        BackGround.Play();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void PlayAudio(AudioSource audio)
    {
        audio.Play();
    }
    public void PlayMusic1()
    {
        BackGround.UnPause();
    }
    public void StopMusic1()
    {
        BackGround.Pause();
    }
    public void PlayMusic2()
    {
        MusicBoss.Play();
    }
    public void StopMusic2()
    {
        MusicBoss.Stop();
    }
    public void UIkillPlayer()
    {
        MurioPlayer.Play();
        MusicBoss.Stop();
        BackGround.Stop();
    }
}
