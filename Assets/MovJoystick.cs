using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovJoystick : MonoBehaviour
{
    public Joystick joystick;
    public int velocidad;
    public Rigidbody2D rb;
    public bool confisicas;
    public GameObject SoundCorrer;
    public PersonajeMovimiento player;
    public float HorizontalJ;
    public bool movement = true;
    Vector2 direction;
    private void Update()
    {
        HorizontalJ = joystick.Horizontal;
        if (!movement) HorizontalJ = 0;
        if (HorizontalJ < 0.0f)
        {
            transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        }
        else if (HorizontalJ > 0.0f)
        {
            transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        }
        player.Animator.SetBool("Runing", HorizontalJ != 0.0f);
    }
    private void FixedUpdate()
    {
        direction = Vector2.right * joystick.Horizontal;
        if (confisicas)
        {
            rb.AddForce(direction * velocidad * Time.fixedDeltaTime, ForceMode2D.Impulse);
        }
        else
        {
            gameObject.transform.Translate(direction * velocidad * Time.deltaTime);
        }    
    }
}
