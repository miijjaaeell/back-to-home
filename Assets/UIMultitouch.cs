using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIMultitouch : MonoBehaviour
{
    public Text btnText;

    public MultiTocuh C1;
    public MultiTocuh C2;
    public MultiTocuh C3;
    public MultiTocuh C4;
    public MultiTocuh C5;
    public MultiTocuh C6;
    public MultiTocuh C7;

    public bool EstaActivo;
    private void Start()
    {
        EstaActivo = true;
    }
    public void btnActivarTouch()
    {
        if (EstaActivo==false)
        {
            C1.enabled = false;
            C2.enabled = false;
            C3.enabled = false;
            C4.enabled = false;
            C5.enabled = false;
            C6.enabled = false;
            C7.enabled = false;
            btnText.text = "ACTIVAR MULTITOUCH";
            EstaActivo = true;
        }
        else if (EstaActivo)
        {
            C1.enabled = true;
            C2.enabled = true;
            C3.enabled = true;
            C4.enabled = true;
            C5.enabled = true;
            C6.enabled = true;
            C7.enabled = true;
            btnText.text = "DESACTIVAR MULTITOUCH";
            EstaActivo = false;
        }
    }
}
