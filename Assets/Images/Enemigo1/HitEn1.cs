using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitEn1 : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.CompareTag("Player"))
        {
            CinemaChineShake.Instance.ShakeCamera(5f, .1f);
        }
    }
}
