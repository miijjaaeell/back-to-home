using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BOSSUI : MonoBehaviour
{

    public GameObject bossPanel;
    public GameObject Puerta2;
    public static BOSSUI instance;

    private void Awake()
    {
        if (instance==null)
        {
            instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        bossPanel.SetActive(false);
        Puerta2.SetActive(false);
    }
    public void BossActivator()
    {
        AudioManager.instance.StopMusic1();
        AudioManager.instance.PlayMusic2();
        bossPanel.SetActive(true);
        Puerta2.SetActive(true);
    }
    public void BossDesactivador()
    {
        Invoke("findGame", 4);
        AudioManager.instance.StopMusic2();
        AudioManager.instance.PlayMusic1();
        bossPanel.SetActive(false);
        Puerta2.SetActive(false);
    }
    public void findGame()
    {
        SceneManager.LoadScene("Final");
    }
}
