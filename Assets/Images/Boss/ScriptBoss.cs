using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScriptBoss : MonoBehaviour
{
    public float maxHealth = 10f;
    public float currentHealth;
    private SpriteRenderer spr;
    public Image VidaImg;
    public GameObject DeadEffect;
    public Material blink, original;
    // Start is called before the first frame update
    public static ScriptBoss instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    void Start()
    {
        currentHealth = maxHealth;
        spr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        DamageBoss();
    }
    public void DamageBoss()
    {
        VidaImg.fillAmount = currentHealth / maxHealth;
    }

    public void TakeDamage(int damge)
    {
        
        currentHealth -= damge;
        Debug.Log("fill " + VidaImg.fillAmount);
        EnemyColor();
        if (currentHealth <= 0)
        {
            TimeR.instanciar.FinTiempo();
            Instantiate(DeadEffect, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
        Debug.Log("Vida del boss" + transform.position+ Quaternion.identity);
    }
    public void EnemyColor()
    {
        Color color = new Color(255 / 255f, 80 / 255f, 80 / 255f);
        spr.color = color;
        //spr.material = blink;
        Invoke("ActivaColor", 0.3f);
    }
    void ActivaColor()
    {
        spr.color = Color.white;
        //spr.material = original;
    }
    private void OnDestroy()
    {
        BOSSUI.instance.BossDesactivador();
    }
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            col.SendMessage("EnemyKnokBack", transform.position.x);
        }
    }
}
