using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiCamera : MonoBehaviour
{
    public static UiCamera instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    public Transform player;
    public float xPos, yPos, zPos;
    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3(player.position.x + 0, player.position.y -0.15f, -10);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(player.position.x + 0, player.position.y - 0.15f, -10);
    }
    void camerasalto()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            transform.position = new Vector3(player.position.x + 0.3f, player.position.y - 0.22f, -10);
        }
        else
        {
            transform.position = new Vector3(player.position.x + 0, player.position.y - 0.15f, -10);
        }
    }
}
