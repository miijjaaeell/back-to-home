using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PersonajeMovimiento : MonoBehaviour
{
    public static PersonajeMovimiento instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }

    public float JumpForce=5f, speed,maxSpeed;
    private Rigidbody2D Rigidbody2D;
    public Animator Animator;
    private float Horizontal;
    public bool Ground;
    public bool movement = true;
    private bool Jump;
    private SpriteRenderer spr;
    // Attaque
    public Transform attackPoint;
    public float attackRange = 0.5f;
    public LayerMask enemyLayers;
    public LayerMask BossLayers;
    public int AttackDamage = 1;
    public float attackRatte = 2f;
    float nextAttackTime = 0f;
    //llaves
    private int ContKey = 0;
    public GameObject PuertaSalaBos;
    //vida 
    public int maxHealth = 5;
    public int currentHealth;
    public VidaPlayer Vida_Canvas;
    public GameObject DeadEffect;
    public GameObject PanelGameOver;
    //sonidoCorrer
    public GameObject SoundCorrer;

    public GameObject Flame;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        Vida_Canvas = GameObject.FindObjectOfType<VidaPlayer>();
        maxHealth = PlayerPrefs.GetInt("vida",5);
        currentHealth = maxHealth;
        Vida_Canvas.CambioVida(currentHealth);
        Rigidbody2D = GetComponent<Rigidbody2D>();
        Animator = GetComponent<Animator>();
        spr = GetComponent<SpriteRenderer>();
        //
    }
    // Update is called once per frame
    void Update()
    {
        if (Time.time>=nextAttackTime)
        {
            if (Input.GetKeyDown(KeyCode.F))
            {
                Attack();
                AttackBoss();
                nextAttackTime = Time.time + 1f / attackRatte;
            }
        }

        Horizontal = Input.GetAxisRaw("Horizontal");
        if (!movement) Horizontal = 0;
        if (Horizontal < 0.0f) transform.localScale = new Vector3(-1.0f, 1.0f, 1.0f);
        else if (Horizontal > 0.0f) transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        Animator.SetBool("Runing", Horizontal != 0.0f);


        //if(Horizontal<0.0f) Flip.flipX = true;
        //else if(Horizontal > 0.0f) Flip.flipX = false;


        if ((Input.GetKeyDown(KeyCode.A)||Input.GetKeyDown(KeyCode.D))&&Ground)
        {
            Instantiate(SoundCorrer);
        }
        Debug.DrawRay(transform.position, Vector3.down * 1.7f, Color.blue);
        if (Physics2D.Raycast(transform.position, Vector3.down, 1.7f))
        {
            if (!Ground)
            {
                AudioManager.instance.PlayAudio(AudioManager.instance.caida);
            }
            Ground = true;
            Animator.SetBool("jump2", false);
            Animator.SetBool("jump", false);
        }
        else
        {
            Animator.SetBool("jump2",true);
            Animator.SetBool("jump", true);
            Ground = false;
        }
        if (Input.GetKeyDown(KeyCode.Z)&&Ground)
        {
            jump();
        }
    }
    public void jump()
    {
        Rigidbody2D.velocity = new Vector2(Rigidbody2D.velocity.x, 0);
        Rigidbody2D.AddForce(Vector2.up*JumpForce, ForceMode2D.Impulse);
        AudioManager.instance.PlayAudio(AudioManager.instance.Salto);
    }
    private void FixedUpdate()
    {
        Vector3 fixedVelocity = Rigidbody2D.velocity;
        fixedVelocity.x *= 0.75f;

        Rigidbody2D.AddForce(Vector2.right * speed * Horizontal);
        float limitSpeed = Mathf.Clamp(Rigidbody2D.velocity.x, -maxSpeed, maxSpeed);
        Rigidbody2D.velocity = new Vector2(limitSpeed, Rigidbody2D.velocity.y);

        if(Ground)
        {
            Rigidbody2D.velocity = fixedVelocity;
        }

        //Rigidbody2D.velocity = new Vector2(Horizontal*speed, Rigidbody2D.velocity.y);

        if (Jump)
        {
            Rigidbody2D.velocity = new Vector2(Rigidbody2D.velocity.x, 0);
            Rigidbody2D.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
            Jump = false;
        }
    }
    public void EnemiJump()
    {
        Jump = true;
    }
    public void MovD()
    {
        //if (Event.current.Equals(Event.KeyboardEvent(KeyCode.A.ToString())))
        //    Debug.Log("Space key is pressed.");
        //Input.GetButton("left");
    }
    public void MovA()
    {
        //Input.GetAxis("izq");
        //Input.GetButton("rigth");
        //Input.GetButtonDown("rigth");
    }
    public void animacion()
    {
       // Animator.SetBool("Runing", false);
    }
    void Die()
    {
        Instantiate(DeadEffect, transform.position, Quaternion.identity);
        //die animation
    }
    public void EnemyKnokBack(float enemyPosX)
    {
        //camera
        //
        Jump = true;
        float side = Mathf.Sign(enemyPosX - transform.position.x);
        Rigidbody2D.AddForce(Vector2.left* side * JumpForce, ForceMode2D.Impulse) ;
        //
        AudioManager.instance.PlayAudio(AudioManager.instance.DanoPlyer);
        //
        movement = false;
        Color color = new Color(255/255f, 80/255f, 80/255f);
        spr.color = color;
        Invoke("ActivaMovimiento", 0.5f);
        //vida
        VidaPLayer();
        Die();
    }
    void VidaPLayer()
    {
        currentHealth -= 1;
        PlayerPrefs.SetInt("vida", currentHealth);
        Vida_Canvas.CambioVida(currentHealth);
        if (currentHealth <= 0)
        {
            Time.timeScale = 0f;
            AudioManager.instance.UIkillPlayer();
            PanelGameOver.SetActive(true);
        }
    }
    void ActivaMovimiento()
    {
        movement = true;
        spr.color = Color.white;
    }
    public void AttackFlame()
    {
        GameObject spell = Instantiate(Flame, transform.position, Quaternion.identity);
    }
    public void Attack()
    {
        //Animacion de Attaque
        Animator.SetTrigger("attack");
        AudioManager.instance.PlayAudio(AudioManager.instance.ATaire);
        //Detectamos Enemigo
        Collider2D[] hitEnemies= Physics2D.OverlapCircleAll(attackPoint.position, attackRange, enemyLayers);
        foreach (Collider2D enemy in hitEnemies)
        {
            AudioManager.instance.PlayAudio(AudioManager.instance.ATalEnemigo);
            enemy.GetComponent<DanoEnemigo>().TakeDamage(AttackDamage);
        }
    }

    public void AttackBoss()
    {
        Animator.SetTrigger("attack");
        AudioManager.instance.PlayAudio(AudioManager.instance.ATaire);
        Collider2D[] hitBoss = Physics2D.OverlapCircleAll(attackPoint.position, attackRange, BossLayers);
        foreach (Collider2D enemy in hitBoss)
        {
            AudioManager.instance.PlayAudio(AudioManager.instance.ATalEnemigo);
            enemy.GetComponent<ScriptBoss>().TakeDamage(AttackDamage);
        }
    }
    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;
        Gizmos.DrawWireSphere(attackPoint.position, attackRange);
    }
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "llave")
        {
            ContKey += 1;
            Debug.Log(ContKey);
            if (ContKey == 2)
            {
                Destroy(PuertaSalaBos);
                Debug.Log("sala del jefe activa");
            }
        }
        if (col.gameObject.tag == "corazon")
        {
            if (currentHealth<5)
            {
                currentHealth += 1;
                Vida_Canvas.CambioVida(currentHealth);
                Debug.Log("vida "+ currentHealth);
                Destroy(col.gameObject);
            }
        }
    }
}
