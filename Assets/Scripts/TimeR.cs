using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TimeR : MonoBehaviour
{
    public static TimeR instanciar;
    private void Awake()
    {
        instanciar = this;
    }
    public Text Crono;
    private TimeSpan tiempoCrono;
    private bool timerBool;
    private float tiempoTrans;

    void Start()
    {
        Crono.text = "00:00";
       
        timerBool = false;
    }
    public void IniciarTiempo()
    {
        //Debug.Log("Timer " + PlayerPrefs.SetString("time", "0"));
        timerBool = true;
        tiempoTrans = float.Parse( PlayerPrefs.GetString("timer", "0"), System.Globalization.CultureInfo.InvariantCulture);
        //PlayerPrefs.GetString("timer", "0");
        //tiempoTrans = float.Parse("0");
        //Debug.Log(" " + PlayerPrefs.GetString("timer", "0"));
        StartCoroutine(ActUpdate());
    }
    public void FinTiempo()
    {
        timerBool = false;
    }
    IEnumerator ActUpdate()
    {
        while(timerBool)
        {
            tiempoTrans += Time.deltaTime;
            tiempoCrono = TimeSpan.FromSeconds(tiempoTrans);
            string tiempoCronoStr = " " + tiempoCrono.ToString("mm':'ss'.'ff");
            Crono.text = tiempoCronoStr;
            PlayerPrefs.SetString("timer", tiempoTrans.ToString());
            yield return null;
        }
    }
}
