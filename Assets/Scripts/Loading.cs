using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using System;


public class Loading : MonoBehaviour
{
    public TextMeshProUGUI textoprogreso;
    //public Image slider;
    public Slider sliderPro;
    float  Valor;
    float tiempo = 3f;
    float contadorTiempo;
    void Start()
    {
        contadorTiempo = 0;
        string leveToLoad = LevelLoader.nextLevel;
        StartCoroutine(this.MakeTheLoad(leveToLoad));
    }
    private void Update()
    {
        if (contadorTiempo<=tiempo)
        {
            contadorTiempo = contadorTiempo + Time.deltaTime;
            sliderPro.value = contadorTiempo / tiempo;
            Valor = contadorTiempo / tiempo;
            textoprogreso.text = (Convert.ToInt32(100 * Valor)).ToString() + "%";
        }
    }
    IEnumerator MakeTheLoad(string level)
    {
        sliderPro.gameObject.SetActive(true);
        yield return new WaitForSeconds(2.9f);
        AsyncOperation operation = SceneManager.LoadSceneAsync(level);
        while(operation.isDone==false)
        {
            yield return null;
        }
    }
}
