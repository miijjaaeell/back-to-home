using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePosition : MonoBehaviour
{
    public int IncioPartida = 0;
    public static SavePosition instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    public void SaveP()
    {
        IncioPartida = 1;
        PlayerPrefs.SetFloat("posx", transform.position.x);
        PlayerPrefs.SetFloat("posy", transform.position.y);
    }
    public Vector2 LoadP()
    {
        float x=PlayerPrefs.GetFloat("posx", -4.24f);
        float y=PlayerPrefs.GetFloat("posy", -0.64f);
        return new Vector2(x, y);
    }
    // Start is called before the first frame update
    void Start()
    {
        transform.position = LoadP();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
