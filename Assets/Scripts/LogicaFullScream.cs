using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LogicaFullScream : MonoBehaviour
{
    public Toggle toggle;
    public TMP_Dropdown ResolucionDrop;
    Resolution[] resoluciones;
    void Start()
    {
        if (Screen.fullScreen)
        {
            toggle.isOn = true;
        }
        else
        {
            toggle.isOn = false;
        }
        RevisarResolucion();
    }
    public void RevisarResolucion()
    {
        resoluciones = Screen.resolutions;
        ResolucionDrop.ClearOptions();
        List<string> opciones = new List<string>();
        int resolucionActual = 0;
        for (int i = 0; i < resoluciones.Length; i++)
        {
            string opcion = resoluciones[i].width + " x " + resoluciones[i].height;
            opciones.Add(opcion);

            if (Screen.fullScreen && resoluciones[i].width == Screen.currentResolution.width && resoluciones[i].height == Screen.currentResolution.height)
            {
                resolucionActual = i;
            }
        }
        ResolucionDrop.AddOptions(opciones);
        ResolucionDrop.value = resolucionActual;
        ResolucionDrop.RefreshShownValue();
        ResolucionDrop.value = PlayerPrefs.GetInt("Res", 0);
    }
    public void CambiarResolucion(int indiceResolucion)
    {
        PlayerPrefs.SetInt("Res", ResolucionDrop.value);
        Resolution resolution = resoluciones[indiceResolucion];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }
    public void ActivarPantallaCompleta(bool pantalla)
    {
        Screen.fullScreen = pantalla;
    }
}
