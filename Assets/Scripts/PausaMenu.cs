using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PausaMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;
    public GameObject PaseMenuUI;
    public GameObject GOopciones,Objetos;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (GameIsPaused)
            {
                Resume();
                GOopciones.SetActive(false);
                Objetos.SetActive(true);
            }
            else
            {
                Pause();
            }
        }
    }
    public void Resume()
    {
        PaseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        Objetos.SetActive(true);
        GameIsPaused = false;
    }
    void Pause()
    {
        SavePosition.instance.SaveP();
        PaseMenuUI.SetActive(true);
        Objetos.SetActive(false);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }
    public void loadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("Menu");
        AudioManager.instance.StopMusic1();
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void btnPause()
    {
        if (GameIsPaused)
        {
            Resume();
            GOopciones.SetActive(false);
            Objetos.SetActive(true);
        }
        else
        {
            Pause();
        }
    }
}
