using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class llavelv1 : MonoBehaviour
{
    public static llavelv1 instance;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
    }
    public Text textLlave;
    public float LimArriba = 0, LimInferior = 0, Velocidad = 0;
    Vector2 dirY = Vector2.up;
    public GameObject Puertalv2,camara1,camara2;
    public  bool puerta=false;

    //player
    public PersonajeMovimiento Player;
    void Update()
    {
        //Puertalv2 = transform.Translate(Vector3.down * Velocidad * Time.deltaTime);
        transform.Translate(dirY * Time.deltaTime * Velocidad);
        if (transform.position.y >= LimArriba)
        {
            dirY = Vector2.down;
        }
        if (transform.position.y <= LimInferior)
        {
            dirY = Vector2.up;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            Player.animacion();
            Player.transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            Player.enabled = false;
            textLlave.text = "FELICIDADES ENCONTRASTE LA PRIMER LLAVE \nDEBLOQUEASTE EL NIVEL 2 ";
            StartCoroutine(Camara());
            StartCoroutine(PlayerMovimiento());
        }
    }
    IEnumerator PlayerMovimiento()
    {
        yield return new WaitForSeconds(4f);
        Player.transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
        Player.enabled = true;
        textLlave.text = " ";
        camara2.SetActive(false);
        camara1.SetActive(true);
        puerta = false;
        Destroy(gameObject);
    }
    IEnumerator Camara()
    {
        yield return new WaitForSeconds(2f);
        camara1.SetActive(false);
        camara2.SetActive(true);
        puerta = true;
    }
}
