using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MostrarTimercreditos : MonoBehaviour
{
    public Text timer;
    // Start is called before the first frame update
    void Start()
    {
        float tiempoTrans = float.Parse(PlayerPrefs.GetString("timer", "0"), System.Globalization.CultureInfo.InvariantCulture);
        TimeSpan tiempoCrono = TimeSpan.FromSeconds(tiempoTrans);
        string tiempoCronoStr = " " + tiempoCrono.ToString("mm':'ss'.'ff");
        timer.text = tiempoCronoStr;
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
