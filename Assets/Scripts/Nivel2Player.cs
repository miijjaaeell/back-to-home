using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Nivel2Player : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.tag == "Player")
        {
            PlayerPrefs.GetFloat("posx", -4.5f);
            PlayerPrefs.GetFloat("posy", -0.6f);
            LevelLoader.LoadLevel("Nivel22");
        }
    }
}
