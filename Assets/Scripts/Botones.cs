using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using UnityEngine.SceneManagement;

public class Botones : MonoBehaviour
{
    public void IniciarPartida()
    {
        LevelLoader.LoadLevel(PlayerPrefs.GetString("nivel","Tutorial"));
    }
    public void tutorial()
    {
        LevelLoader.LoadLevel("Tutorial");
    }
    public void Exit()
    {
        Application.Quit();
    }
    public void BorrarDatos()
    {
        PlayerPrefs.DeleteAll();
    }
}
