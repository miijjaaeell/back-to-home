using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public VidaPlayer Vida_Canvas;
    // Start is called before the first frame update
    void Start()
    {
        PlayerPrefs.SetString("nivel", SceneManager.GetActiveScene().name);
        //Vida_Canvas.CambioVida(PersonajeMovimiento.instance.currentHealth);
        Time.timeScale = 1f;
        if (TimeR.instanciar!=null)
        {
            TimeR.instanciar.IniciarTiempo();
        }
        
        Vida_Canvas.CambioVida(PlayerPrefs.GetInt("vida"));
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
