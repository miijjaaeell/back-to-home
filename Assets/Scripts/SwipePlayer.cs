using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwipePlayer : MonoBehaviour
{
    public PersonajeMovimiento Player;
    Vector2 PosicionInicial;
    public float SwipeMinY; //0.5
    public float SwipeMinX; //0.5
    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                PosicionInicial = touch.position;
            }
            else if (touch.phase == TouchPhase.Ended)
            {
                float swipeVertical = (new Vector3(0, touch.position.y, 0) - new Vector3(0, PosicionInicial.y, 0)).magnitude;
                float swipeHorizontal = (new Vector3(0, touch.position.x, 0) - new Vector3(0, PosicionInicial.x, 0)).magnitude;
                if (swipeVertical > SwipeMinY && swipeVertical > swipeHorizontal)
                {
                    float u = Mathf.Sign(touch.position.y - PosicionInicial.y);
                    if (u > 0)
                    {
                        if (Player.Ground)
                        {
                            Player.jump();
                        }

                    }
                    if (u < 0)
                    {
                        if (Player.Ground)
                        {
                            Player.jump();
                        }
                    }
                }
            }
        }
    }
}
