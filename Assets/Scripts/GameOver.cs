using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public void ReiniciarEscena()
    {
        PlayerPrefs.DeleteAll();
        PlayerPrefs.SetFloat("posx", -4.5f);
        PlayerPrefs.SetFloat("posy", -0.6f);
        PlayerPrefs.SetString("timer", "0");
        TimeR.instanciar.FinTiempo();
        SceneManager.LoadScene("Nivel1");
    }
    public void Exit()
    {
        SceneManager.LoadScene("Menu");
    }
}
