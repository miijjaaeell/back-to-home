using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivacionLV : MonoBehaviour
{
    public float Velocidad = 2;
    Vector2 dirY = Vector2.up;
    // Update is called once per frame
    void Update()
    {
        if (llavelv1.instance.puerta==true)
        {
            transform.Translate(dirY * Time.deltaTime * Velocidad);
        }
    }
}
