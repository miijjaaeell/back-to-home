using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deathefect : MonoBehaviour
{
    ScriptBoss boss;
    public GameObject DeadEffect;
    void Start()
    {
        boss = GetComponent<ScriptBoss>();
    }
    public void FixedUpdate()
    {
        if (boss.currentHealth <= 0)
        {
            Instantiate(DeadEffect, transform.position, Quaternion.identity);
        }
    }

    // Update is called once per frame
    //private void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.CompareTag("Player"))
    //    {
    //        if (boss.currentHealth <= 0)
    //        {
    //            Instantiate(DeadEffect, transform.position, Quaternion.identity);
    //            Destroy(gameObject);
    //        }
    //    }
    //}
}
