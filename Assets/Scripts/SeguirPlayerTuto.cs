using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguirPlayerTuto : MonoBehaviour
{
    public GameObject follow;
    public Vector2 minCamaraPos, maxCamaraPos;

    public float smoothtime;
    private Vector2 velocity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float posX = Mathf.SmoothDamp(transform.position.x, follow.transform.position.x, ref velocity.x, smoothtime);
        float posY = Mathf.SmoothDamp(transform.position.y, follow.transform.position.y, ref velocity.y, smoothtime);

        transform.position = new Vector3(Mathf.Clamp(posX, minCamaraPos.x, maxCamaraPos.x), Mathf.Clamp(posY, minCamaraPos.y, maxCamaraPos.y), transform.position.z);
    }
}
